//
//  Services.swift
//  MapsDirection
//
//  Created by Edison Effect on 7/31/18.
//  Copyright © 2018 balitax. All rights reserved.
//
// Autor: Sandra Herrera.
// Verison: 0.2.
// Fecha de actualización: 21/08/18.


import Foundation
import UIKit
import Alamofire


//MARK: ********* Protocolo *********
protocol ServicesDelegate
{
    
    /// Respuesta del servicio cuando todo salio bien
    ///
    /// - Parameters:
    ///   - dictionaryData: Información en json que se obtiene del request
    ///   - typeService: Identificador del servicio al cual se llamó
    func parserData(dictionaryData: NSDictionary, typeService : Int)
    
    /**
     Respuesta cuando fue un error del servicio.
     
     @param strError Mensaje del error en string del servicio.
     @param typeService Identificador del servicio al cual se llamo.
     */
    /// Respuesta cuando fue un error del servicio
    ///
    /// - Parameters:
    ///   - strError: Mensaje del error en string del servicio
    ///   - typeService: Identificador del servicio al cual se llamó
    ///   - intCode: (En objective c no dice xd)
    func error (strError: NSString, typeService: Int, intCode: Int)
}
//MARK: ********* Clase *********
public class Services: ServicesDelegate
{
    
    /// Delegado Respuesta del servicio cuando todo salió bien
    ///
    /// - Parameters:
    ///   - dictionaryData: Información en json que se obtiene del request
    ///   - typeService: Identificador del servicio al cual se llamó
    func parserData(dictionaryData: NSDictionary, typeService: Int) {
        print("Delegado parserData de services")
    }
    
    /// Delegado Respuesta de error del servicio
    ///
    /// - Parameters:
    ///   - strError: Mensaje de error
    ///   - typeService: Tipo de servicio de la solicitud
    ///   - intCode:
    func error(strError: NSString, typeService: Int, intCode: Int) {
        print("Delegado error de services")
    }
    //MARK: ********* Variables *********
    /// Constante para definir la URL del server
    let SERVER_NAME : String = "https://www.levelgas.com/PHPServices/"
    
    
    /// Contendrá la url para solicitar el servicio
    var strUrl = ""
    
    
    /// Diccionario para los encabezados
    var dictHeaders :[String:String]? = [:]
    
    
    /// Información que se envía en el cuerpo del servicio.(parameters)
    var dataHTTPBody :[String:String] = [:]
    
    
    /// Nombre de usuario en dado caso que el servicio requiera autenticacion
    var username = ""
    
    
    /// Contraseña dado caso que el servicio requiera autenticación
    var password = ""
    
    
    /// Se activa cuando se envía información al servidor.Se libera cuando llegan los datos
    var boolSendService : Bool?
    
    /// Delegado para acceder a las funciones de los servicios
    var delegate: ServicesDelegate?
    
    enum TypeHeaders : Int {
        //                                                      //Header con parametros por default.
        case ZDHeaderDefault = 0,
        //                                                      //Header con JSON
        ZDHeaderJson,
        //                                                      //Header con Json y autorización.
        ZDHeaderJsonBasicAuth
    }
    
    /// Se le asignan que tipo de encabezados tendrá el web service para configurarlo
    var typeHeaders: TypeHeaders?
    
    
    enum TypeServices: Int {
        case ZDServiceDefault = 0
        //                                                  //1.Crear un usuario
        case ZDServiceCreateUser = 1
        //                                                  //2.Iniciar sesión.
        case ZDServiceLoginUser = 2
        //                                                  //3.Cerrar sesión.
        case ZDServiceLogoutUser = 3
        //                                                  //4.Obtiene la versión de datos.
        case ZDServiceGetDataVersion = 4
        //                                                  //5.Obtiene toda la información del usuario.
        case ZDServiceGetAccountInfo = 5
        //                                                  //6.Direcciondes del google maps
        case ZDServiceGetAddresGoogleMaps = 6
        //                                                  //7.Agregar tanque.
        case ZDServiceAddTank = 7
        //                                                  //8.Actualiza la calibración.
        case ZDServiceUpdateCalibrate = 8
        //                                                  //9.Inserta los niveles del tanque.
        case ZDServiceSetTankLevels = 9
        //                                                  //10.Actualiza la información del tanque.
        case ZDServicesUpdateInfoTank = 10
        //                                                  //11.Elimina el tanque.
        case ZDServicesDeleteTank = 11
        //                                                  //12.Cambia el password.
        case ZDServiceChangePassword = 12
        //                                                  //13.Actualiza la información del usuario.
        case ZDServiceUpdateInfoUser = 13
        //                                                  //14.Recuperar la contraseña.
        case ZDServicePasswordForgot = 14
    }
    
    /// Para asignar que tipo de servicio fue el que se envió
    var typeServices :TypeServices?
    
    // **********************METODOS*************************
    
    //MARK: ********* Método GET *********
    
    /// GET Request
    ///
    /// - Parameters:
    ///   - dictParameters: json del servicio
    ///   - nameService: Nombre del servicio al que se realiza el request
    ///   - serviceInt: Identificador Int(enumeracion) del servicio que se solicita
    func GETRequest(nameService :String, serviceInt : Int)
    {
        sendWebServiceHTTPMethodTypeTo( service: serviceInt, strNameService: nameService, dictionaryParams: [:], strMethod: .get, boolEncoding : false)
    }
    //MARK: ********* Método PUT *********
    /// PUT Request
    ///
    /// - Parameters:
    ///   - dictParameters: json del servicio
    ///   - nameService: Nombre del sercicio al que se realiza el request
    ///   - typeHeader: Identificador Int(enumeración) del servicio al que se solicita
    func PUTRequest(dictParameters :[String:String], nameService :String, typeHeader : Int)
    {
        sendWebServiceHTTPMethodTypeTo(service: typeHeader, strNameService: nameService, dictionaryParams: dictParameters, strMethod: .put, boolEncoding: true)
    }
    
    //MARK: ********* Método POST *********
    /// POST Request
    ///
    /// - Parameters:
    ///   - dictParameters: cuerpo del servicio
    ///   - nameService: Nombre del servicio al que se realiza el request
    ///   - strService: Identificador Int(enumeración) del servicio al que se solicite
    func POSTRequest(dictParameters :[String:String], nameService :String, strService : Int)
    {
        sendWebServiceHTTPMethodTypeTo(service: strService, strNameService: nameService, dictionaryParams: dictParameters, strMethod: .post, boolEncoding: true)
    }
    
    //MARK: ********* Método DELETE *********
    /// DELETE Request
    ///
    /// - Parameters:
    ///   - dictParameters: cuerpo del servicio
    ///   - nameService: Nombre del sercicio al que se realiza el request
    ///   - typeHeader: Identificador Int(enumeración) del servicio al que se solicite
    func DELETErequest(dictParameters :[String:String], nameService :String, typeHeader : Int)
    {
        sendWebServiceHTTPMethodTypeTo(service: typeHeader, strNameService: nameService, dictionaryParams: dictParameters, strMethod: .delete, boolEncoding: true)
    }
    //MARK: ********* Método ... *********
    /// Inicializa URL del servicio
    ///
    /// - Parameters:
    ///   - service: Identificador del servicio
    ///   - strNameService: Nombre del servicio (Url que se concatena con el SERVER_NAME para formar la solicitud http
    ///   - dictionaryParams: Cuerpo del servicio
    ///   - strMethod: tipo de solicitud http
    ///   - boolEncoding: Bandera para saber cuando inicializar un JSONEncoding en la solicitud
    func sendWebServiceHTTPMethodTypeTo( service: Int, strNameService: String, dictionaryParams :[String:String], strMethod : HTTPMethod, boolEncoding : Bool)
    {
        strUrl = strNameService
        print("URL: \(strUrl)")
        dataHTTPBody = dictionaryParams
        sendHTTPWebServices(serviceInt: service)
        requestHTTP(strMethodType: strMethod ,dictParameters: dictionaryParams, url: SERVER_NAME + strUrl, headers: dictHeaders!, username: username, passwd: password, boolEncoding: boolEncoding, serviceInt: service)
    }
    //MARK: ********* Si no hay internet, no puede solicitar ningun servicio *********
    
    /// Para validar si puede continuar solicitando un servicio mientras tenga una conexión a internet
    ///
    /// - Parameter serviceInt: Servicio del que llega la solicitud
    func sendHTTPWebServices(serviceInt : Int ) {
        if isConnectedToInternet()
        {
            boolSendService = true
            configurationSession(service: typeHeaders!.rawValue)
        }
        else
        {
            boolSendService = false
            self.delegate?.error(strError: "Error de conexion", typeService: serviceInt, intCode: 0)
        }
    }
    //MARK: ********* Configura el HEADER*********
    /// Define el tipo de encabezado para la solicitud http
    ///
    /// - Parameter service: servicio del que llega la solicitud
    func configurationSession(service : Int) {
        switch service {
        case Services.TypeHeaders.ZDHeaderJson.hashValue:
            dictHeaders = ["Content-Type": "application/json"]
        case Services.TypeHeaders.ZDHeaderJsonBasicAuth.hashValue:
            dictHeaders = ["Content-Type": "application/json","x-api-key":"c7155b48a55918a01626535972e3034a"]
        default:
            dictHeaders = ["Content-Type": "application/json"]
        }
    }
    
    //MARK: ********* Método principal para solicitud de servicios HTTP *********
    /// Método principal para solicitar un servicio
    ///
    /// - Parameters:
    ///   - strMethodType: Tipo de request http
    ///   - dictParameters: diccionario con el body de la solicitud
    ///   - url: direccion del servicio
    ///   - headers: encabezados del servicio
    ///   - username: usuario para autenticacion
    ///   - passwd: password para autenticacion
    ///   - boolEncoding: decide si el JSON se va codificar default
    ///   - serviceInt: tipo de servicio de solicitud
    func requestHTTP(strMethodType : HTTPMethod,dictParameters :[String:String], url :String, headers:[String:String],username:String, passwd:String, boolEncoding:Bool, serviceInt : Int)
    {
        if  boolEncoding
        {
            Alamofire.request(url, method: strMethodType,
                              parameters: dictParameters,
                              encoding: JSONEncoding.default,
                              headers: headers)
                .authenticate(user:username, password: passwd)
                
                .responseJSON{ response in
                    
                    switch response.result
                    {
                    case .success(let bien as NSDictionary):
                        //                        print("Respuesta: \(response.result)")
                        self.delegate?.parserData(dictionaryData: bien, typeService: serviceInt)
                    case .failure(let error):
                        print(error)
                        self.delegate?.error(strError: "hubo un error", typeService: serviceInt, intCode: 0)
                    default:
                        print("No entro a ninguno")
                    }
            }
        }
        else
        {
            Alamofire.request(url, method: strMethodType,
                              parameters: dictParameters,
                              headers: headers)
                .authenticate(user:username, password: passwd)
                
                .responseJSON{ response in
                    switch response.result
                    {
                    case .success(let bien as NSDictionary):
                        //                        print("Respuesta: \(response.result)")
                        self.json(jsonData: bien)
                        self.delegate?.parserData(dictionaryData: bien, typeService: serviceInt)
                    case .failure(let error):
                        print(error)
                        self.delegate?.error(strError: "Hubo un error", typeService: serviceInt, intCode: 0)
                        
                    default:
                        print("No entro a ninguno")
                    }
            }
        }
        
    }
    
    /// funcion muestra para parsear los resultados de request
    ///
    /// - Parameter jsonData: Diccionario con la respuesta del request
    func json(jsonData : NSDictionary){
        guard let error = jsonData["error"] else {
            return
        }
        print("Se encontro un error")
        print(error)
    }
    //Para comprobar si hay conexión a internet
    /// Funcion para comprobar si hay conexión a internet
    ///
    /// - Returns: valor booleano
    func isConnectedToInternet() -> Bool{
        return NetworkReachabilityManager()!.isReachable
    }
    /// Funcion que indica si hay conexión a internet
    ///
    /// - Returns: valor booleano
    func thereInternetConnection() -> Bool {
        if self.isConnectedToInternet()
        {
            print("Yes Internet is available")
            return true
        }
        else
        {
            print("No, internet is not available")
            return false
        }
    }
    
}



